#include "thread_op.h"

void
*print_c ( void *c )
{
    // Cast 'c' void pointer to TThread Pointer
    TThread *p = (TThread*) c;

    // Get Thread ID
    pthread_t thread_id = pthread_self();
    p->th_id = thread_id;

    for (int i=0; i<p->count; i++)
    {
        printf ( "%i.- %c\n",
                i,
                p->param );
        usleep( DELAY );
    }
    return NULL;
}
