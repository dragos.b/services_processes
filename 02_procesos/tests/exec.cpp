#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

enum TProcess { CHILD, PARENT, END };

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
print_pid ( const char *name )
{
    char pid[100] = " PID ";
    strcat ( pid, name );
    printf ( "%s -> %i\n", name, getpid() );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' #  EXEC  # ' " );

    pid_t pid = fork();
    int status;

    if ( pid == CHILD )
    {
        const char *arg_list[] = { "wait", "10", NULL };
        print_pid ( "CHILD" );
        usleep    ( 10000000 );
        execvp    ( "./wait", (char* const*) arg_list );
        printf    ( "ERROR IN CHILD PROCESS\n\n" );
    }

    else if ( pid >= PARENT )
    {
        printf ( "PARENT PROCESS\n" );
        print_pid( "PARENT" );
        wait   ( &status );
        printf ( "FINISH PARENT PROCESS\n" );
        printf ( "CHILD PID -> %i\n\n", pid );
        print_pid( "PARENT" );

    }
    else
        printf ( "ERROR\n\n" );

    return EXIT_SUCCESS;
}
