#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

enum TProcess { CHILD, PARENT, END };

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' #  Wait  # ' " );
    pid_t pid;

    pid = fork();
    int counter = 0;

    if ( argc > 1 )
        counter = atoi( argv[1] );

    if ( pid == CHILD )
    {
        for (int i=0; i<10; i++)
        {
            counter++;
            printf( "CHILD COUNTER -> %i\n", counter );
            usleep( 1000000 );
        }
        printf( "\n\n" );
        exit ( counter );

    } else if ( pid >= PARENT )
    {
        int status;
        counter += 10;
        wait( &status );
        printf( "PARENT COUNTER -> %i\n\nSUM OF PARENT & CHILD COUNTER -> %i\n\n",
                counter,
                counter + WEXITSTATUS(status) );
    }
    else
        printf ( "Error when calling 'frok'\n\n" );

    return EXIT_SUCCESS;
}
