#include "stack.h"
#include "process.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "files.h"


int sig_usr_count = 0;
int process_type;

void handler( int sig )
{
    switch (sig)
    {
        case SIGUSR1:
            sig_usr_count++;
            break;
        case SIGCHLD:
            if ( process_type == CHILD )
            {
                
                int fd = create_file();
                writeStackFile( fd );
                exit( sig_usr_count );
            }
            break;
        case SIGINT:
            if ( process_type == PARENT )
                printf( "No está permitido el uso de '^C'\n" );
            break;
    }
}

void event()
{
    signal( SIGUSR1, &handler );
    signal( SIGCHLD, &handler );
    signal( SIGINT,  &handler );
}

void process()
{
    printf  ( "\
    HIJO\t-> Calcula Primos\n\
    PADRE/HIJO\t-> SIGUSR1 counter\n\
    HIJO\t-> SIGCHLD finaliza y escribe en un fichero\n\
    SIGINT\t-> desactivado (^C)\n\n\n" );

    pid_t child_pid = fork();
    int status;

    event();
    if ( child_pid == CHILD ) {
        printf( "PID HIJO -> %i\n\n", getpid() );
        process_type = CHILD;

        pushStack(2);
        pushStack(3);
        pushStack(5);

        primeNumber();
    }

    else if ( child_pid >= PARENT )
    {
        printf( "PID PADRE-> %i\n\n", getpid() );
        process_type = PARENT;
        waitpid( child_pid, &status, 0 );
        sig_usr_count += WEXITSTATUS( status );
        printf ( "SIGUSR1 (Padre e Hijo) recibido %i veces \n\n", sig_usr_count );
    }
}
