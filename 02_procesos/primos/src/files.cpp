/*
 * =====================================================================================
 *       Filename:  files.cpp
 *    Description:
 *
 *        Created:  01/11/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#include <stdlib.h>
#include <unistd.h>
#include "files.h"

int
create_file () {

    char filename[] = "./tmp_XXXXXX";
    int file_descriptor = mkstemp ( filename );

    return file_descriptor;
}

int
write_file ( int file_d, const char *buff, size_t size ) {

    write ( file_d, buff, size );           // Write buffer string in the file.
    return file_d;
}

