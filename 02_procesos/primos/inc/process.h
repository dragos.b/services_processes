/*
 * =====================================================================================
 *       Filename:  process.h
 *    Description:
 *
 *        Created:  31/10/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __PROCESS_H__
#define __PROCESS_H__

#ifdef __cplusplus
extern "C" {
#endif

#define CHILD    0
#define PARENT   1


void handler (int sig);
void event   ();
void process ();

#ifdef __cplusplus
}

#endif
#endif
