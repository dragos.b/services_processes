/*
 * =====================================================================================
 *       Filename:  stack.h
 *    Description:
 *
 *        Created:  31/10/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __STACK_H__
#define __STACK_H__


#ifdef __cplusplus
extern "C" {
#endif

void pushStack      ( unsigned number );
void writeStackFile ( int fd );
void deleteStack();
void primeNumber();

#ifdef __cplusplus
}
#endif
#endif
