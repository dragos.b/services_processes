/*
 * =====================================================================================
 *       Filename:  files.h
 *    Description:
 *
 *        Created:  01/11/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __FILES_H__
#define __FILES_H__

#ifdef __cplusplus
extern "C" {
#endif

int create_file ();
int write_file  ( int fd, const char *buff, size_t size );

#ifdef __cplusplus
}
#endif


#endif
