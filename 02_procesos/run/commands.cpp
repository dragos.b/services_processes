/*
 * =====================================================================================
 *       Filename:  commands.cpp
 *    Description:
 *
 *        Created:  20/10/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "commands.h"


void
spawn ( const char *command, char **args ) {
    pid_t rv_fork = fork(); // Create child process

    if ( rv_fork != 0 ) {
        printf ( "I'm the parent process %i \n\n", getpid() );
        wait ( NULL );
    }
    else {
        execvp ( command, args );
        fprintf ( stderr, "Error in exec function\n" );
        abort ();
    }


}
