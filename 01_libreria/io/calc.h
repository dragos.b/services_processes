/*
 * =====================================================================================
 *       Filename:  calc.h
 *    Description:  header file
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __CALC_H__
#define __CALC_H__

#ifdef __cplusplus
extern "C"
{
#endif

  double sum  ( double, double );
  double sub  ( double, double );
  double mul  ( double, double );
  double divi ( double, double );
  double mod  ( double, double );

#ifdef __cplusplus
}
#endif

#endif
