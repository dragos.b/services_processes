/*
 * =====================================================================================
 *       Filename:  calc.cpp
 *    Description:
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#include "calc.h"

double sum  (double op1, double op2)  { return op1 + op2; }
double sub  (double op1, double op2)  { return op1 - op2; }
double mul  (double op1, double op2)  { return op1 * op2; }
double divi (double op1, double op2)  { return op1 / op2; }
double mod  (double op1, double op2)  { return (int) op1 % (int) op2; }
