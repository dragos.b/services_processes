/*
 * =====================================================================================
 *       Filename:  out.cpp
 *    Description:  Output file
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */
#include "out.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
print_operation ( double op1, double op2, double result, char operand ) {
    printf ( "\t\t%.1lf %c %.1lf = %4.1lf\n", op1, operand, op2, result );
}
