/*
 * =====================================================================================
 *       Filename:  out.h
 *    Description:  out header file
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __OUT_H__
#define __OUT_H__


#ifdef __cplusplus
extern "C"
{
#endif

    void banner ( const char* );
    void print_operation ( double , double , double , char );
    //                    number1   number2  result   operand
#ifdef __cplusplus
}
#endif


#endif
