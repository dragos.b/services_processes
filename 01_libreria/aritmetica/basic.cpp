/*
 * =====================================================================================
 *       Filename:  basic.cpp
 *    Description:
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */
#include "arit.h"

double add ( double op1, double op2 ) { return op1 + op2; }
double sub ( double op1, double op2 ) { return op1 - op2; }
