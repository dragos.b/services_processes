/* * =====================================================================================
 *       Filename:  advanced.cpp
 *    Description:
 *
 *        Created:  23/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */
#include "arit.h"

double mult ( double op1, double op2 ) { return op1 * op2; }
double divi  ( double op1, double op2 ) { return op1 / op2; }
int mod  ( int op1, int op2 ) { return op1 % op2; }
