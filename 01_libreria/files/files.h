/*
 * =====================================================================================
 *       Filename:  files.h
 *    Description:
 *
 *        Created:  29/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __FILES_H__
#define __FILES_H__

#ifdef __cplusplus
extern "C"
{
#endif
    int create_file ( const char* );
    int write_file  ( int, const char*, size_t );



#ifdef __cplusplus
}
#endif

#endif

