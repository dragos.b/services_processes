/*
 * =====================================================================================
 *       Filename:  interface.h
 *    Description:
 *
 *        Created:  28/09/20
 *         Author:  disced (), dragosdisce@gmail.com
 * =====================================================================================
 */

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#ifdef __cplusplus
extern "C"
{
#endif
    void banner   ( const char*);
    void usage    ( FILE* );
    void mainLoop ( int, char*[], const char*, const char* );

#ifdef __cplusplus
}
#endif


#endif
